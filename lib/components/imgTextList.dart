// 图文列表
import 'package:flutter/material.dart';

class ImgText extends StatelessWidget {
  const ImgText({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(10),
      children: <Widget>[
        Image.asset(
          'images/bhxqtd.jpg',
          fit: BoxFit.cover,
          // height: 500,
        ),
        Container(
          padding: const EdgeInsets.all(5),
          height: 44,
          child: const Text('图文标题1',
              textAlign: TextAlign.center, style: TextStyle(fontSize: 20)),
        )
      ],
    );
  }
}
