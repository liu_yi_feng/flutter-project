// 新闻列表
import 'package:flutter/material.dart';

class Newslist extends StatelessWidget {
  const Newslist({super.key});

  @override
  Widget build(Object context) {
    return ListView(
      // padding: const EdgeInsets.all(10),
      //自定义padding 
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      children:  <Widget> [
        ListTile(
          leading: Image.network('https://www.itying.com/images/flutter/1.png'),
          // 主标题
          title: const Text('我是主标题'),
          // 副标题
          subtitle: const Text('我是副标题'),
        )
      ],
    );
  }
}