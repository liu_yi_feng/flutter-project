// 网格布局
import 'package:flutter/material.dart';

class GridBox extends StatelessWidget {
  const GridBox({super.key});
  @override
  Widget build(BuildContext context) {
    // return GridView.count(
    //   crossAxisCount: 5,
    //   children: const [
    //     Icon(Icons.abc),
    //     Icon(Icons.ac_unit),
    //     Icon(Icons.vaccines_rounded),
    //     Icon(Icons.tab_sharp),
    //     Icon(Icons.ac_unit_rounded),
    //     Icon(Icons.h_mobiledata_rounded),
    //     Icon(Icons.javascript_rounded),
    //     Icon(Icons.one_x_mobiledata_rounded),
    //     Icon(Icons.wallet_giftcard),
    //     Icon(Icons.baby_changing_station),
    //   ],
    // );
     return GridView.extent(
      maxCrossAxisExtent: 120, // 子元素最大宽度
      children: const [
        Icon(Icons.abc),
        Icon(Icons.ac_unit),
        Icon(Icons.vaccines_rounded),
        Icon(Icons.tab_sharp),
        Icon(Icons.ac_unit_rounded),
        Icon(Icons.h_mobiledata_rounded),
        Icon(Icons.javascript_rounded),
        Icon(Icons.one_x_mobiledata_rounded),
        Icon(Icons.wallet_giftcard),
        Icon(Icons.baby_changing_station),
      ],
    );
  }
}
