import 'package:flutter/material.dart';

class List extends StatelessWidget {
  const List({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: const <Widget> [
        ListTile(
          // 图标
          leading: Icon(Icons.home),
          // 标题
          title: Text('首页')
        ),
        Divider(), // 一根线类似hr
        ListTile(
          // 更改图标颜色
          leading: Icon(Icons.home, color: Color.fromRGBO(176, 46, 46, 1),),
          title: Text('首页')
        ),
        Divider(), // 一根线类似hr
        ListTile(
          leading: Icon(Icons.home),
          title: Text('首页'),
          // 后面加东西 , 加一个向右的箭头
          trailing: Icon(Icons.chevron_right_sharp),
        ),
        Divider(), // 一根线类似hr
      ],
    );
  }
  
}