// 定义一个动态列表
import 'package:flutter/material.dart';
import '../res/data.dart';

class ActiveLis extends StatelessWidget {
  const ActiveLis({super.key});
  // // 定义一个动态数据一, 列表是个widget 类型
  // List<Widget> _dyList() {
  //   // ignore: non_constant_identifier_names
  //   List<Widget> Arr = [];
  //   for (int i = 0; i < 10; i++) {
  //     Arr.add(ListTile(
  //       title: Text('我是動態列表$i'),
  //     ));
  //   }
  //   return Arr;
  // }
  // 从假数据中获取数据, 定义方法2
  List<Widget> _getList() {
    List<Widget> Arr = [];
    // 使用 map 转换 Map 的 entries 到 List<Widget>
    Arr = listData.asMap().entries.map((entry) {
      final index = entry.key;
      final item = entry.value;
      pirnt('cessfe$index');
      return ListTile(
        leading: Image.network(item['imgUrl']),
        title: Text(item['title']),
        subtitle: Text(item['content']),
      );
      // 需要调用toList()转换数组要不是(1,2,3)这种类型的
    }).toList();
    return Arr;
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: _getList(),
    );
  }

  void pirnt(item) {}
}
