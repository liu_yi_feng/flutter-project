import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter02/components/list.dart';
import 'package:flutter02/components/newsList.dart';
import 'package:flutter02/components/imgTextList.dart';
import 'package:flutter02/components/activeList.dart';
import 'package:flutter02/components/gridBox.dart';
void main() {
  // MaterialApp是所有组件的根组件
  runApp(MaterialApp(
    // appBar 可以配置顶部的导航
    home: Scaffold(
      appBar: AppBar(
        title: const Text('我是导航标题'),
      ),
      // body 内容区域
      // body: const MyApp(),
      body: const MyApp(),
    ),
  ));
}

// 自定义组件myApp 继承无状态类
class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(Object context) {
    return const Center(
        // 必传项child,
        // textDirection下有浪哥值ltr从左向右, rtl右向左
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          // 使用Expanded包裹List，使其占据Row的可用空间
          child: List(),
        ),
        Text('下面是新闻列表'),
        Expanded(
            // 使用Expanded包裹List，使其占据Row的可用空间
            child: Newslist()),
        Expanded(
          child: ImgText(),
        ),
        Expanded(
          child: SpList(),
        ),
        // SpList(),
        Expanded(
          child: ActiveLis(),
        ),
        Expanded(
          child: GridBox(),
        ),
      ],
    ));
  }
}

// 水平列表
class SpList extends StatelessWidget {
  const SpList({super.key});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        // 外面容器高度设置内部水平列表高度

        child: ListView(
      // 设置为水平高度自适应, 设置为垂直排列宽度自适应
      scrollDirection: Axis.horizontal, // 修改为水平列表
      shrinkWrap: true, // 添加此属性以收缩高度
      physics: const ClampingScrollPhysics(),
      //  physics: const NeverScrollableScrollPhysics(),
      padding: const EdgeInsets.all(10),
      children: <Widget>[
        Container(
          width: 81,
          // 设置背景色
          decoration: const BoxDecoration(
            color: Color.fromRGBO(255, 173, 20, 9),
          ),
        ),
        Container(
          width: 81,
          // 设置北京干in是
          decoration: const BoxDecoration(
            color: Color.fromRGBO(227, 100, 16, 0.969),
          ),
        ),
        Container(
          width: 81,
          // 设置北京干in是
          decoration: const BoxDecoration(
            color: Color.fromRGBO(27, 49, 188, 0.969),
          ),
        ),
        Container(
          width: 81,
          // 设置背景色
          decoration: const BoxDecoration(
            color: Color.fromRGBO(255, 173, 20, 9),
          ),
        ),
        Container(
          width: 81,
          // 设置北京干in是
          decoration: const BoxDecoration(
            color: Color.fromRGBO(227, 100, 16, 0.969),
          ),
        ),
        Container(
          width: 81,
          // 设置北京干in是
          decoration: const BoxDecoration(
            color: Color.fromRGBO(27, 49, 188, 0.969),
          ),
        ),
        Container(
          width: 81,
          // 设置背景色
          decoration: const BoxDecoration(
            color: Color.fromRGBO(255, 173, 20, 9),
          ),
        ),
        Container(
          width: 81,
          // 设置北京干in是
          decoration: const BoxDecoration(
            color: Color.fromRGBO(227, 100, 16, 0.969),
          ),
        ),
        Container(
          width: 81,
          // 设置北京干in是
          decoration: const BoxDecoration(
            color: Color.fromRGBO(27, 49, 188, 0.969),
          ),
        )
      ],
    ));
  }
}
