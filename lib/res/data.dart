// 自定义列表假数据
List listData = [
  {
    "title": "我是标题1",
    "content": "我是内容1",
    "imgUrl": "https://www.itying.com/images/flutter/1.png"
  },
  {
    "title": "我是标题2",
    "content": "我是内容2",
    "imgUrl": "https://www.itying.com/images/flutter/2.png"
  },
  {
    "title": "我是标题3",
    "content": "我是内容3",
    "imgUrl": "https://www.itying.com/images/flutter/3.png"
  },
  {
    "title": "我是标题4",
    "content": "我是内容4",
    "imgUrl": "https://www.itying.com/images/flutter/4.png"
  },
  {
    "title": "我是标题5",
    "content": "我是内容5",
    "imgUrl": "https://www.itying.com/images/flutter/5.png"
  },

];
